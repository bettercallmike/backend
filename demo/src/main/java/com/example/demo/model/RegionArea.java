package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "region_areas")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegionArea implements Serializable {
    @Id
    @Column(name = "region_name", nullable = false, length = 100)
    private String id;

    @Column(name = "region_area", nullable = false, precision = 15, scale = 2)
    private BigDecimal regionArea;


}