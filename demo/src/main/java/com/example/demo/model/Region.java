package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "regions")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Region implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "continent_id", nullable = false)
//    @JsonIgnoreProperties("regions")
    @JsonIgnore
    private Continent continent ;

    @OneToMany(mappedBy = "region" ,fetch = FetchType.EAGER)
    private Collection<Country> countries = new LinkedHashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Region region = (Region) o;
        return id != null && Objects.equals(id, region.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}