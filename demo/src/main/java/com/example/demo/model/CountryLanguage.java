package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "country_languages")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CountryLanguage implements Serializable {
    @EmbeddedId
    private CountryLanguageId id =  new CountryLanguageId();

    @Column(name = "official")
    private boolean official ;


    @MapsId("languageId")
    @ManyToOne()
    @JoinColumn(name = "language_id")
    private Language language;


    @MapsId("countryId")
    @ManyToOne()
    @JoinColumn(name = "country_id")
    private Country country;

}