package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Setter
@Getter
public class CountryStatId implements Serializable {

    @Column(name = "country_id", nullable = false)
    private Integer countryId;
    @Column(name = "year", nullable = false)
    private Integer year;


    @Override
    public int hashCode() {
        return Objects.hash(year, countryId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CountryStatId entity = (CountryStatId) o;
        return Objects.equals(this.year, entity.year) &&
                Objects.equals(this.countryId, entity.countryId);
    }
}