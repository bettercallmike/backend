package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "continents")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Continent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "continent_id", nullable = false)
    private Integer id;


    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "continent",fetch = FetchType.EAGER)
    @JsonIgnoreProperties("continent")
        Collection<Region> regions  = new LinkedHashSet<>()  ;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Continent continent = (Continent) o;
        return id != null && Objects.equals(id, continent.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}