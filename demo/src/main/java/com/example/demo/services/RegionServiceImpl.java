package com.example.demo.services;

import com.example.demo.model.Country;
import com.example.demo.model.Region;
import com.example.demo.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
@Service
public class RegionServiceImpl {

     private RegionRepository regionRepository;

     @Autowired
    public RegionServiceImpl(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

  public List<Country> findCountriesByRegionName(String name){


     Region region= regionRepository.findByName(name)
                                    .orElseThrow(NoSuchElementException::new);

      List<Country> countries = (List<Country>) region.getCountries();
      return countries;
  }


}
