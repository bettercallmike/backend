package com.example.demo.services;


import com.example.demo.dto.OverviewDTO;
import com.example.demo.dto.OverviewListDTO;
import com.example.demo.model.Continent;
import com.example.demo.model.Country;
import com.example.demo.model.CountryStat;
import com.example.demo.model.Region;
import com.example.demo.repository.ContinentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContinentServiceImpl {

 ContinentRepository continentRepository;
@Autowired
    public ContinentServiceImpl(ContinentRepository continentRepository) {
        this.continentRepository = continentRepository;
    }


    public OverviewListDTO findAllContinentsByPage(Integer Page, int size){
        Pageable paging = PageRequest.of(Page, size);
        org.springframework.data.domain.Page<Continent> continentsPaged =
                                                         continentRepository.findAll(paging);
        List<Continent> continents = continentsPaged.getContent();
        List<OverviewDTO> overviewDTOList =new ArrayList<>();

      for(Continent continent : continents ){
           OverviewDTO overViewDTO = new OverviewDTO();

           overViewDTO.setContinentName(continent.getName());
           Collection<Region> regions = continent.getRegions();

           Set<String> regionNames = regions.stream().map(Region::getName).collect(Collectors.toSet());
           overViewDTO.setRegionNames(regionNames);

           overviewDTOList.add(overViewDTO);
      }

        long count = continentRepository.count();
        System.out.println("count is :" +count);
        System.out.println("count is :" +count);

        OverviewListDTO overviewListDTO = new OverviewListDTO(count,overviewDTOList);

        return overviewListDTO;
    }




}
