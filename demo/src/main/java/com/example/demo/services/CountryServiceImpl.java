package com.example.demo.services;

import com.example.demo.dto.LanguageDTO;
import com.example.demo.model.Country;
import com.example.demo.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CountryServiceImpl {


    private CountryRepository countryRepository;


    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Transactional(readOnly = true)
    public List<Country> findAllCountrys() {
        return countryRepository.findAll();

    }

    public List<Country> findAllbyPage(Integer page, int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Country> pagedCusto = countryRepository.findAll(paging);
        List<Country> custo = pagedCusto.getContent();
        return custo;
    }


    public List<LanguageDTO> getLanguagesOfCountry(String name) {

        List<LanguageDTO> languages = new ArrayList<>(0);

        Country country = countryRepository.findByName(name).orElseThrow(NoSuchElementException::new);

        languages = countryRepository.findAllLanguagesOfCountry(country.getId())
                .stream().collect(Collectors.toList());


        return languages;

    }

    public long count() {
        return countryRepository.count();
    }


}

