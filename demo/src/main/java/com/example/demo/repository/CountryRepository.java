package com.example.demo.repository;

import com.example.demo.dto.LanguageDTO;
import com.example.demo.model.Country;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {


    @Query(value = "SELECT l.language  FROM countries as c " +
                   "INNER JOIN country_languages as c_l ON c.country_id = c_l.country_id" +
                   " INNER JOIN languages as l ON l.language_id = c_l.language_id" +
                   " WHERE c.country_id= :countryId",nativeQuery = true)
    Collection<LanguageDTO> findAllLanguagesOfCountry(Integer countryId);

   Optional<Country> findByName(String name);

   Optional<Country> findById(Integer id);


}