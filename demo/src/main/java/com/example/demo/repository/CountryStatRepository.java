package com.example.demo.repository;

import com.example.demo.model.CountryStat;
import com.example.demo.model.CountryStatId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryStatRepository extends JpaRepository<CountryStat, CountryStatId> {



}