package com.example.demo.dto;

import com.example.demo.model.Country;
import lombok.*;

import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CountriesPageDTO implements Serializable {
    List<Country> countries;
    long count;


}
