package com.example.demo.dto;


import java.io.Serializable;

public interface LanguageDTO extends Serializable {

    String getLanguage();

}
