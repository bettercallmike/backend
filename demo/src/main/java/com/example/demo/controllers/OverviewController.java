package com.example.demo.controllers;


import com.example.demo.dto.OverviewListDTO;
import com.example.demo.services.ContinentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class OverviewController {


    ContinentServiceImpl continentService;

     @Autowired
    public OverviewController(ContinentServiceImpl continentService) {
        this.continentService = continentService;
    }

    @RequestMapping(value = "/overview/{page}/{size}",method= RequestMethod.GET)
    public ResponseEntity<OverviewListDTO> getCountryLanguages(@PathVariable("page") Integer page,@PathVariable("size") int size){
     OverviewListDTO   overviewDTO=   continentService.findAllContinentsByPage(page,size);

        return new ResponseEntity<>(overviewDTO, HttpStatus.OK);
    }

}
