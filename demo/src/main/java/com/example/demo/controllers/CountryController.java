package com.example.demo.controllers;


import com.example.demo.dto.LanguageDTO;
import com.example.demo.model.Country;
import com.example.demo.services.CountryServiceImpl;
import com.example.demo.dto.CountriesPageDTO;
import com.example.demo.services.RegionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class CountryController {

    private CountryServiceImpl countryService;
    private RegionServiceImpl regionService;

    @Autowired
    public CountryController(CountryServiceImpl countryService, RegionServiceImpl regionService) {
        this.countryService = countryService;
        this.regionService = regionService;
    }

    @RequestMapping(value = "/countries/{page}/{size}",method= RequestMethod.GET)
 public  ResponseEntity<CountriesPageDTO>   getAllCountries(@PathVariable("page") Integer page,
                                                            @PathVariable("size") int size){
       long count =countryService.count();
       List<Country>  countries = countryService.findAllbyPage(page, size);

       CountriesPageDTO countriesPageDTO  = new CountriesPageDTO(countries,count);

      return new ResponseEntity<>(countriesPageDTO, HttpStatus.OK);
    }




    @RequestMapping(value = "/countries/language/{name}",method= RequestMethod.GET)
    public  ResponseEntity<List<LanguageDTO>>   getCountryLanguages(@PathVariable("name") String name){

       List<LanguageDTO>  languages =
                        countryService.getLanguagesOfCountry(name);

        return new ResponseEntity<>(languages, HttpStatus.OK);
    }



    @RequestMapping(value = "/countries/region/{name}",method= RequestMethod.GET)
    public  ResponseEntity<List<String>>   getCountryNames(@PathVariable("name") String name){

        List<String>  countryNames = regionService.findCountriesByRegionName(name)
                                                  .stream().map(Country::getName)
                                                  .collect(Collectors.toList());


        return new ResponseEntity<>(countryNames, HttpStatus.OK);
    }

//    @RequestMapping(value = "/countries/stats/{countryId}",method= RequestMethod.GET)
//    public  ResponseEntity<Set<CountryStat>>   getStatsOfCountryById(@PathVariable("countryId") Integer countryId){
//
//        Set<CountryStat>  countryStats =
//                countryService.fintStatsOfCountryById(countryId);
//
//        return new ResponseEntity<>(countryStats, HttpStatus.OK);
//    }


}
